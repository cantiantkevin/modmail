const Discord = require("discord.js")
const client = new  Discord.Client()
const { token, server_id, modmail_viewing_role_id, modmail_category_name } = require("./setting.json")

client.on("ready", () => {
    console.log("bot en ligne")
    client.user.setActivity("MP pour contacter le staff", {
        type: "listening"
      });
})
client.on('message', async message => {

    if(message.author.bot) return;
const server = client.guilds.cache.get(server_id);

    if(!server) return;
    if(message.channel.type == "dm") {
       if(!server) return;
        if(!server.members.cache.has(message.author.id)) return message.channel.send(`Tu dois d'abord rejoindre le serveur : ${server.name} pour utiliser ModMail !`);


        const channel_name = message.author.id
        let everyoneRole = server.roles.cache.find(r => r.name === '@everyone')
        let moderatorRole = server.roles.cache.find(r => r.id === modmail_viewing_role_id)
        let category = await server.channels.cache.find(c => c.name == modmail_category_name && c.type == 'category')
        if(!category) {
            category = await server.channels.create(modmail_category_name, {
            type: 'category',
            permissionOverwrites: [
              {
                    id: server.roles.everyone.id,
                    deny: ['VIEW_CHANNEL'],
              },
              {
                    id: moderatorRole.id,
                    allow: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
              },
              {
                    id: client.user.id,
                    allow: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
              },
            ],
          })
        }
        let channel = await server.channels.cache.find(c => c.name == channel_name && c.type == 'text')
        if(!channel) {
            channel = await server.channels.create(channel_name, {
            type: 'text',

          })


        const embed = new Discord.MessageEmbed()
        .setTitle('Nouveau Ticket')
        .setColor('Purple')
        .setDescription('Envoyez un message dans ce salon pour y répondre.\nPour fermer le ticket =close .\nPour envoyer un message sans l\'envoyer à l\'utilisateur incluez `&` dans la réponse .')
        await channel.send(embed);
        const embed2 = new Discord.MessageEmbed()
        .setTitle('Nouveaux ticket ouvert')
        .setColor('Purple')
        .setDescription('Tu as ouvert un ticket!')
        await message.channel.send(embed2);
        }
        if(channel && category) channel.setParent(category.id)

          if(message.attachments.first()){
            const embed1 = new Discord.MessageEmbed()
            .setTitle('Message Reçu')
            .setColor('Purple')
            .setDescription(`
            **Auteur :** 
            ${message.author.tag}
            **message :**
            ${message.content}
            **URL :**
            ${message.attachments.array()[0].url}
            **Image ( si c'est une image ) :**`)
            .setTimestamp(new Date())
            .setImage(message.attachments.array()[0].url)
            .setFooter(client.user.tag)
            channel.send(embed1);

            const embed3 = new Discord.MessageEmbed()
            .setTitle('Message envoyé.')
            .setColor('Purple')
            .setDescription(`Message envoyé au staff:
            ${message.content}
            **URL :**
            ${message.attachments.array()[0].url}
            **Image ( si c'est une image ) :**`)
            .setImage(message.attachments.array()[0].url)
            .setTimestamp(new Date())
            .setFooter(client.user.tag)
            await message.channel.send(embed3)
          } else {
            const embed1 = new Discord.MessageEmbed()
            .setTitle('Message Reçu')
            .setColor('Purple')
            .setDescription(`
            **Auteur :** 
            ${message.author.tag}
            **message :**
            ${message.content}`)
            .setTimestamp(new Date())
            .setFooter(client.user.tag)
            channel.send(embed1);

            const embed3 = new Discord.MessageEmbed()
            .setTitle('Message envoyé.')
            .setColor('Purple')
            .setDescription(`Message envoyé au staff:
            ${message.content}`)
            .setTimestamp(new Date())
            .setFooter(client.user.tag)
            await message.channel.send(embed3);
          }


    } else {

            const user_id2 = message.channel.name
            const user_id = message.guild.members.cache.get(user_id2)
            const moderateur = message.author.tag

            if(message.content == `=close`) {
               message.channel.send("Le channel sera suprimé dans 10 secondes")
               setTimeout(async() => {
                message.channel.delete()
                const closeuserembed = new Discord.MessageEmbed()
                .setTitle('Ticket fermé.')
                .setColor('Purple')
                .setDescription(`Pour ouvrir un autre ticket, envoyez-moi un message!`)
                .setTimestamp(new Date())
          .setFooter(client.user.tag)
                user_id.send(closeuserembed).catch();
               }, 10000);

                return;
            } else if(message.content.match("&")) return;
            if(!user_id) return;
            message.delete()




           if(message.attachments.first()){
            const embed2 = new Discord.MessageEmbed()
            .setTitle('Message Reçu')
            .setColor('Purple')
            .setDescription(`**Auteur :** 
            ${moderateur}
            **message :**
            ${message.content}
            **URL :**
            ${message.attachments.array()[0].url}
            **Image ( si c'est une image ) :**`)
            .setImage(message.attachments.array()[0].url)
            .setTimestamp(new Date())
          .setFooter(client.user.tag)
            user_id.send(embed2);




            const embed3 = new Discord.MessageEmbed()
            .setTitle('Message envoyé.')
            .setColor('Purple')
            .setDescription(`**Message envoyer à :**
            ${user_id.user.tag}
            **message :**
            ${message.content}
            **URL :**
            ${message.attachments.array()[0].url}
            **Image ( si c'est une image ) :**
            `)
            .setImage(message.attachments.array()[0].url)
            .setTimestamp(new Date())
          .setFooter(client.user.tag)
            message.channel.send(embed3);
           } else {
            const embed2 = new Discord.MessageEmbed()
            .setTitle('Message Reçu')
            .setColor('Purple')
            .setDescription(`**Auteur :** 
            ${moderateur}
            **message :**
            ${message.content}`)
            .setTimestamp(new Date())
          .setFooter(client.user.tag)
            user_id.send(embed2);

            const embed3 = new Discord.MessageEmbed()
            .setTitle('Message envoyé.')
            .setColor('Purple')
            .setDescription(`**Message envoyé à :**
            **message :**
            ${message.content}`)
            .setTimestamp(new Date())
          .setFooter(client.user.tag)
            message.channel.send(embed3);
           }


    };





})



client.login(token)
